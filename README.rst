.. SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>
.. SPDX-License-Identifier: CC-BY-SA-4.0

======
Noktra
======

Noktra (Not Only Kde TRAnslations), translation extraction and management.

Initially developed by the KDE community, it manages the extraction of
translations from different types of sources. The types of handled sources
is managed through a set of plugins and can be easily extended.

Features
--------

* TODO
