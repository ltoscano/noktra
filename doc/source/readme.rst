.. SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>
.. SPDX-License-Identifier: CC-BY-SA-4.0

.. include:: ../../README.rst
