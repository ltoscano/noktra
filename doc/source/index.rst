.. SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>
.. SPDX-License-Identifier: CC-BY-SA-4.0

.. noktra documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

======================================
Welcome to the documentation of noktra
======================================

Contents:

.. toctree::
   :maxdepth: 2

   readme

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
