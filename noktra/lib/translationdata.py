# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2022 Luigi Toscano <luigi.toscano@tiscali.it>

import datetime
from pathlib import Path
import logging
from typing import Optional

import polib

LOG = logging.getLogger(__name__)


class TranslationTemplateData():
    """ Helper class to store the intermediate results of the parsing
    of translation data. It decouples the core of noktra from polib.
    """

    def __init__(self):
        self._template_data = {}

    def add_entry_data(self, context: Optional[str], message: str,
                       file_path: str, line_number: Optional[int] = None,
                       comment: Optional[str] = None):
        """ Register the availability of the specified file_path, optionally
        occurring at line number, and of the specified comment, for the given
        combination of context + message.
        If multiple file_path/line_number occurrences are specified for the
        same (context,message) they are combined. At the same time,
        only the last specified comment is used.
        """
        key = (context, message)
        if key not in self._template_data:
            self._template_data[key] = {
                'path_line': set(),
                'comment': ''
            }

        path_line = (file_path, line_number if line_number else '')

        self._template_data[key]['path_line'].add(path_line)
        # Only the last comment survives. Maybe they should be combined?
        self._template_data[key]['comment'] = comment if comment else ''

    def get_location(self, context: Optional[str], message: str):
        key = (context, message)
        if key not in self._template_data:
            return []
        return list(sorted(self._template_data[key]['path_line']))

    def get_comment(self, context: Optional[str], message: str):
        key = (context, message)
        if key not in self._template_data:
            return ''
        return self._template_data[key]['comment']

    def get_keys(self):
        return self._template_data.keys()

    def save_pot_file(self, out_file_name: Path,
                      project_id: Optional[str] = None) -> Optional[str]:

        # Do not save the file if empty
        if not self._template_data:
            return None

        # initialize the pot file
        pot_file = polib.POFile()
        if not project_id:
            project_id = out_file_name.stem

        pot_file.metadata = {
            'Project-Id-Version': project_id,
            'Report-Msgid-Bugs-To': '',
            'POT-Creation-Date': datetime.datetime.now(
                datetime.timezone.utc).strftime('%Y-%m-%d %H:%M%z'),
            'PO-Revision-Date': 'YEAR-MO-DA HO:MI+ZONE',
            'Last-Translator': 'FULL NAME <EMAIL@ADDRESS>',
            'Language-Team': 'LANGUAGE',
            'MIME-Version': '1.0',
            'Content-Type': 'text/plain; charset=UTF-8',
            'Content-Transfer-Encoding': '8bit',
        }
        for msgctxtid, msgdata in self._template_data.items():
            entry = polib.POEntry(
                msgctxt=msgctxtid[0],
                msgid=msgctxtid[1],
                msgstr='',
                occurrences=sorted(list(msgdata['path_line'])),
                comment=msgdata['comment']
            )
            pot_file.append(entry)

        # TODO: while polib should support Path objects, the support
        # is not official and types-polib does not declare it,
        # which makes type checkers like mypy fail.
        # See https://github.com/izimobil/polib/issues/114
        pot_file.save(str(out_file_name))
        LOG.info('Saved {}'.format(out_file_name))
        return out_file_name.name
