# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Luigi Toscano <luigi.toscano@tiscali.it>

import logging
from pathlib import Path
import shutil
from typing import List

LOG = logging.getLogger(__name__)


def copy_track_files(src_dir: Path, dst_dir: Path, pattern: str) -> List[str]:
    """Copy all the files which matches a specified pattern
    to a specified directory.
    Return the list of files copied (just file names).
    """
    dest_files = []
    for found_file in src_dir.glob(pattern):
        try:
            shutil.copy2(found_file, dst_dir)
            dest_files.append(found_file.name)
        except IOError as exc:
            LOG.error('Something went wrong while copying the files: '
                      '{}'.format(str(exc)))
    return dest_files
