# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>

from collections import namedtuple
import logging
from pathlib import Path
from typing import List

import tomli


LOG = logging.getLogger(__name__)
SOURCE_CONFIG_FILE_NAME = 'noktrasource.toml'

SourceConfiguration = namedtuple(
    'SourceConfiguration', ['handler', 'name', 'config'], defaults=(None, {})
)


class SourcePackage():
    """ Utility class which groups the information for a source
    package (directory, name, list of included files). """

    def __init__(self, source_dir: Path, package_name: str):
        self._source_dir = source_dir
        self._package_name = package_name
        if not package_name:
            self._package_name = Path(source_dir).absolute().name
        self._source_files = self._find_files()

    def _find_files(self):
        return list(Path(self.source_dir).glob('**/*'))

    @property
    def source_files(self) -> List[Path]:
        return self._source_files

    @property
    def source_dir(self) -> Path:
        return self._source_dir

    @property
    def package_name(self) -> str:
        return self._package_name


class SourceConfigurationReader():
    """ The noktra configuration in the source package."""

    def __init__(self, source_package: SourcePackage):
        self._source_config_file = None
        candidate_config = Path(source_package.source_dir,
                                SOURCE_CONFIG_FILE_NAME)
        if candidate_config in source_package.source_files:
            self._source_config_file = candidate_config
        self._config_read = False
        self._config = None

    def _read_config(self):
        if not self._source_config_file:
            return None

        found_config = []
        try:
            with open(self._source_config_file, 'rb') as cf:
                config_file_content = tomli.load(cf)

            for section, content in config_file_content.items():
                # find whether a section (handler) specifies also a title,
                # for ex. "foo-bar" represent the section "foo"
                # for the bar.pot template file.
                section_elements = section.split('-', 1)
                handler = section_elements[0]
                file_name = None
                if len(section_elements) == 2:
                    file_name = section_elements[1]

                found_config.append(
                    SourceConfiguration(handler, file_name, content))
        except Exception as exc:
            LOG.warning('Error while reading {}: {}'.format(
                self._source_config_file, str(exc)))
        finally:
            return found_config

    @property
    def config_items(self):
        if not self._config_read:
            self._config = self._read_config()
            self._config_read = True
        return self._config
