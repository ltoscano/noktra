# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021-2022 Luigi Toscano <luigi.toscano@tiscali.it>

import fileinput
import re
from typing import Dict, List, Optional


def pot_postprocess(pot_files: List, header_values: Optional[Dict] = None):
    """Fix the headers of a pot file according header_values
    """

    if isinstance(pot_files, str):
        pot_files = [pot_files]

    if not pot_files or not isinstance(header_values, dict):
        return

    for pot_file in pot_files:
        # while fileinput.input supports multiple files, it is easier
        # to handle the parsing code below if each file is parsed
        # separately
        with fileinput.input(files=pot_file, inplace=True) as potf:
            # When the first 'msgid ""' line is found, it marks the start
            # of the header section: the line can be printed as it is,
            # and the header search can be tried.
            # When the second 'msgid ""' is found, the header section is
            # finished: the line can be printed, and from now on the
            # the header search must not be performed.
            # If we are outside the header or, being inside the header,
            # the header search did not succeed, the line found
            # can be printed as it is.
            msgid_found = 0
            for line in potf:
                found_item = False
                if msgid_found < 2:
                    if re.match('^msgid "', line):
                        msgid_found += 1
                if msgid_found == 1:
                    for searched_header, new_value in header_values.items():
                        if re.match('^"' + searched_header + ':', line):
                            print('"{}: {}\\n"'.format(searched_header,
                                                       new_value))
                            found_item = True
                            # exit from this loop
                            continue
                if not found_item:
                    print(line, end='')
