# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>

import configparser
from collections import UserDict
from enum import Enum
from importlib_metadata import entry_points
import logging
import pathlib
from typing import Set

from noktra.lib.potutils import pot_postprocess
from noktra.lib.sourcepackage import SourceConfigurationReader
from noktra.lib.sourcepackage import SourceConfiguration
from noktra.lib.sourcepackage import SourcePackage

LOG = logging.getLogger(__name__)


class Operation(Enum):
    EXTRACTION = 1
    INJECTION = 2


class HandlerCollection(UserDict):

    def __init__(self, config_file, requested_handlers=None,
                 ignore_not_found=False):

        self._handlers = {}

        cp = configparser.ConfigParser()
        cp.read_file(open(config_file))

        found_handlers = entry_points(group="noktra.handlers")
        logging.debug('All handlers: {}'.format(found_handlers))

        # if any subset of handlers was requested, make sure they exist
        if requested_handlers:
            missing_handlers = requested_handlers.difference(found_handlers)
            if missing_handlers:
                raise Exception('Requested handlers {} are unknown'.
                                format(missing_handlers))
        else:
            requested_handlers = found_handlers

        # keep track of the not found handler errors, and
        # report them and die, unless ignore_missing is specififed
        not_found_list = []

        # only load the needed handlers - or all of them
        for handler in requested_handlers:
            try:
                self._handlers[handler.name] = handler.load()(cp)
                logging.debug('Loaded handler {}'.format(
                              str(self._handlers[handler.name])))
            except ModuleNotFoundError as not_found_exc:
                not_found_list.append(handler.name)
                logging.warning('Error loading the {} handler: {}'.
                                format(handler.name, str(not_found_exc)))
        if not_found_list and not ignore_not_found:
            raise Exception('Requested handlers {} not found'.
                            format(not_found_list))

        # FIXME: find a better way to parse the section
        # without the fallback
        self._pot_header_fix_config = {}
        try:
            potheaderscp = configparser.ConfigParser(
                default_section='core:filter:potheaderfixer')
            potheaderscp.optionxform = str
            potheaderscp.read_file(open(config_file))
            self._pot_header_fix_config = dict(
                potheaderscp['core:filter:potheaderfixer'])
        except Exception as exc:
            logging.debug('Error while reading potheaderfixer config: {}'.
                          format(str(exc)))

    def get(self, handler):
        return self._handlers[handler]

    def is_requested_handler_valid(self, requested_handler, handlers_called,
                                   operation: Operation):
        """Return true if the requested handler is valid."""

        # Does the requested handler exist?
        # If it does not, not a fatal error, just ignore it
        if requested_handler not in self._handlers.keys():
            logging.warning(
                'Requested unknown handler {}, skipping'.format(
                    requested_handler))
            return False

        # An handler which does not support being called multiple times
        # was just called again, ignore the new request
        if requested_handler in handlers_called and \
            not self.get(requested_handler).support_multiple_calls:
            logging.warning(
                'The requested handler {} can be called only once, '
                'ignoring...'.format(requested_handler))
            return False

        # Was an injection requested but the requested handled does not
        # support injections?
        # Just a debug messages, because in most cases it is expected
        # that handlers don't support injection.
        if operation == Operation.INJECTION:
            if not hasattr(self.get(requested_handler),
                           'inject_translations'):
                logging.debug(
                    'The requested handler {} does not support the requested '
                    'injection operation, skipping...'.format(
                        requested_handler))
                return False

        return True

    def _get_sourceconfiguration_all_handlers(self):
        """ Return all the handlers as a list of SourceConfiguration.
        This is used when the source repository does not contain
        any configuration file, because all handlers are tried."""
        config = []
        for handler in self._handlers.keys():
            config.append(SourceConfiguration(handler, None, {}))
        return config

    def _handlers_loop(self, code_dir, output_dir, package_name,
                       operation: Operation):

        # get the configured handlers for code_dir, if any is defined
        source_package = SourcePackage(code_dir, package_name)
        source_config = SourceConfigurationReader(source_package)

        source_config_items = source_config.config_items
        # if the source configuration contains no configuration
        # ([] means the file was found but it is empty,
        # None means it was not found) then load all handlers
        if source_config_items is None:
            source_config_items = self._get_sourceconfiguration_all_handlers()

        all_processed_files = []
        handlers_called: Set[str] = set()
        for source_requested_handler in source_config_items:

            requested_handler = source_requested_handler.handler

            if not self.is_requested_handler_valid(requested_handler,
                                                   handlers_called, operation):
                continue

            try:
                self.get(requested_handler).sanitize_source_config(
                    source_requested_handler.config)
            except ValueError as exc:
                logging.warning('Configuration error for {}: {}'.format(
                    requested_handler, str(exc)))

            if operation == Operation.INJECTION:
                op_method = self.get(requested_handler).inject_translations
            else:
                op_method = self.get(requested_handler).extract_templates

            logging.debug('Running handler {}'.format(requested_handler))
            extracted_files = op_method(
                source_package, output_dir,
                source_config=source_requested_handler.config,
                catalog_name=source_requested_handler.name)
            all_processed_files.extend(extracted_files)
            handlers_called.add(requested_handler)

        return all_processed_files

    def extract_templates(self, code_dir, templates_dir, package_name):

        all_extracted_pots = self._handlers_loop(
            code_dir, templates_dir, package_name, Operation.EXTRACTION)
        LOG.info('extracted: {}'.format(sorted(all_extracted_pots)))

        # Post-processing for extraction
        try:
            generated_pot_files = list(pathlib.Path(templates_dir).
                                       glob('*.pot'))
            pot_postprocess(generated_pot_files, self._pot_header_fix_config)
        except Exception as exc:
            LOG.info('Error while fixing the pot headers: {}'.format(str(exc)))

    def inject_translations(self, code_dir, translations_dir, package_name):
        changed_files = self._handlers_loop(
            code_dir, translations_dir, package_name, Operation.INJECTION)
        LOG.info('changed: {}'.format(sorted(changed_files)))
