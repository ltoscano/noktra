# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Luigi Toscano <luigi.toscano@tiscali.it>

import argparse
import logging
from pathlib import Path
import sys

from noktra.handlercollection import HandlerCollection


LOG = logging.getLogger(__name__)


class InjectorConfigurationException(Exception):
    pass


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('--ignore-not-found-handlers', action='store_true')
    parser.add_argument('-s', '--source-dir', type=Path, default=Path('.'))
    parser.add_argument('-c', '--config-file', type=Path,
                        default=Path('.').absolute() / 'noktra.conf')
    parser.add_argument('-p', '--package-name', type=str, default=None)
    parser.add_argument('-t', '--translations-dir', type=Path, default=None)

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.WARNING)

    source_dir = args.source_dir
    config_file = args.config_file
    translations_dir = args.translations_dir
    try:
        if not source_dir.is_dir():
            raise InjectorConfigurationException('Code directory not set.')

        if not config_file.is_file():
            raise InjectorConfigurationException(
                'Configuration file not found.')

        if not translations_dir.is_dir():
            raise InjectorConfigurationException(
                'Base translations directory not set.')

    except InjectorConfigurationException as exc:
        sys.stderr.write(str(exc) + '\n\n')
        parser.print_help(file=sys.stderr)
        sys.exit(1)

    package_name = args.package_name

    handlers = HandlerCollection(
        config_file=config_file,
        ignore_not_found=args.ignore_not_found_handlers)

    handlers.inject_translations(source_dir, translations_dir, package_name)


if __name__ == "__main__":
    main()
