# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Luigi Toscano <luigi.toscano@tiscali.it>

from typing import Optional, List, Tuple

from noktra.handlers.base_xml import BaseXMLHandler


class SharedMIMEInfoHandler(BaseXMLHandler):

    _name = 'SharedMIMEInfo'
    _configuration_key = 'sharedmimeinfo'

    _xml_namespace = 'http://www.freedesktop.org/standards/shared-mime-info'

    def default_excluded_re(self) -> List[str]:
        # managed by a more specialized handler
        return [r'.+\.(metainfo|appdata)\.xml$']

    def translatable_patterns(
            self, config_tags: List[str]
            ) -> List[Tuple[str, Optional[str]]]:
        if not config_tags:
            config_tags = ['comment']
        return [("//*[name()='{}']".format(tag.strip()), tag)
                for tag in config_tags]

    def default_source_file(self, package_name) -> Optional[str]:
        return '{}.xml'.format(package_name)
