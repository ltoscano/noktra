# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2022-2023 Luigi Toscano <luigi.toscano@tiscali.it>

from abc import abstractmethod
import pathlib
from typing import Dict, Optional, List, Tuple

import lxml.etree as le

from noktra.handlers.base import BaseInjectorHandler
from noktra.lib.sourcepackage import SourcePackage
from noktra.lib.translationdata import TranslationTemplateData


class BaseXMLHandler(BaseInjectorHandler):

    _name = 'XML'
    _configuration_key = 'xml'
    _xml_namespace = ''

    def default_included_re(self) -> List[str]:
        return [r'.+\.xml$']

    def xml_clean_parsed(self, xml_file):
        """ Return the parsed XML tree of the XML file
            without the translated content. """
        with open(xml_file, 'r') as source_xml:
            parsed_doc = le.parse(source_xml)

        # find and remove all translated XML tags
        for translated_elem in parsed_doc.xpath('//*[@xml:lang]'):
            parent = translated_elem.getparent()
            parent.remove(translated_elem)

        return parsed_doc

    def _recurse_elements(self, element):
        """ Recursively gather the information about "element" and
            its children in the XML tree."""
        found_msgs = []
        if element is None:
            return found_msgs
        # ignore elements with translate=no/false
        if 'translate' in element.attrib:
            translate_value = element.attrib['translate']
            if translate_value.lower() in ['no', 'false']:
                return found_msgs
        clean_txt = ' '.join([el.strip() for el in element.text.split()])
        if clean_txt:
            clean_elementpath = element.getroottree().getelementpath(element)
            # if a namespace is specified, it should be removed
            if self._xml_namespace:
                clean_elementpath = clean_elementpath.replace(
                    '{{{}}}'.format(self._xml_namespace), '')
            found_msgs.append((clean_txt, element.sourceline,
                              clean_elementpath))
        for child in element:
            found_msgs.extend(self._recurse_elements(child))
        return found_msgs

    @abstractmethod
    def translatable_patterns(
            self, config_tags: List[str]
            ) -> List[Tuple[str, Optional[str]]]:
        """Return the list of translatable tags.
        It gets the list of tags coming from configuration."""
        pass

    @abstractmethod
    def default_source_file(self, package_name) -> Optional[str]:
        """The default source file name to look for, if any."""
        pass

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        requested_source = source_config.get(
            'sources', self.default_source_file(source_package.package_name))

        xml_files = self.filter_source_files(
            source_package.source_files,
            specific_files=requested_source)

        # dictionary of translation data for each requested pot
        translation_map: Dict[str, TranslationTemplateData] = {}

        # extract the translations for each .xml file found
        for xml_file in xml_files:

            pot_id = catalog_name if catalog_name else xml_file.stem

            if pot_id not in translation_map:
                translation_map[pot_id] = TranslationTemplateData()

            parsed_doc = self.xml_clean_parsed(xml_file)

            # extract all the translatable XML tags
            config_tags = []
            config_tags_raw = source_config.get('tags', None)
            if config_tags_raw is not None:
                config_tags = config_tags_raw.split(',')
            xml_patterns = self.translatable_patterns(config_tags)

            found_msgs = []
            for pattern, tr_tag in xml_patterns:
                elements_found = parsed_doc.xpath(pattern)

                for element in elements_found:
                    found_msgs.extend(self._recurse_elements(element))

            for msg in found_msgs:
                path_comment = ''
                try:
                    start = 0
                    if msg[2][0] == '/':
                        start = 1
                    path_comment = 'path: {}'.format(msg[2][start:])
                except (TypeError, IndexError):
                    pass
                translation_map[pot_id].add_entry_data(
                    tr_tag, msg[0], xml_file.name, msg[1], path_comment)

        all_pots = []
        for pot_id, translation_data in translation_map.items():
            xml_pot = pot_id + '.pot'

            out_pot_file = pathlib.Path(templates_dir, xml_pot)

            real_out = translation_data.save_pot_file(out_pot_file)
            if real_out:
                all_pots.append(xml_pot)
        return all_pots

    def inject_translations(self, source_package: SourcePackage,
                            translations_dir: str, source_config: dict,
                            catalog_name: Optional[str] = None) -> List[str]:

        # FIXME: placeholder debug
        import inspect
        c_frame = inspect.currentframe()
        assert c_frame is not None
        print(inspect.getargvalues(c_frame))
        return []
