# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>

import logging
from pathlib import Path, PurePath
import shutil
import subprocess
import tempfile
from typing import List, Optional

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage
from noktra.lib.fileutils import copy_track_files

LOG = logging.getLogger(__name__)


def which(executable, default=None):
    ex_path = shutil.which(executable)
    return ex_path if ex_path else default


class LegacyExtractMessagesHandler(BaseHandler):

    _name = 'LegacyExtractMessages'
    _configuration_key = 'legacyextractmessages'

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        tmp_po_dir = tempfile.mkdtemp(prefix='noktra.lem.po')
        tmp_enpo_dir = tempfile.mkdtemp(prefix='noktra.lem.enpo')

        extract_env = {
            'XGETTEXT': which('xgettext', 'xgettext'),
            'MSGCAT': which('msgcat', 'msgcat'),
            'EXTRACTRC': 'perl {} --ignore-no-input'.format(self.config(
                'extractrc', 'extractrc')),
            'EXTRACTATTR': 'perl {}'.format(self.config('extractattr',
                                                        'extractattr')),
            'EXTRACT_GRANTLEE_TEMPLATE_STRINGS': 'python {}'.format(
                self.config('extractgrantlee', 'extractgrantlee')),
            'PREPARETIPS': 'perl {}'.format(self.config('preparetips',
                                                        'preparetips5')),
            'REPACKPOT': 'perl {}'.format(self.config('repackpot',
                                                      'repack-pot.pl')),
            'PACKAGE': source_package.package_name,
            'IGNORE': '.git',
            'podir': tmp_po_dir,
            'enpodir': tmp_enpo_dir
        }
        extract_messages = PurePath(
            self.config('legacyscriptydir', '.'),
            'extract-messages.sh')

        try:
            em = subprocess.run(['bash', str(extract_messages)],
                                capture_output=True,
                                cwd=source_package.source_dir,
                                env=extract_env)
            LOG.debug('Executed {}:{}'.format(str(em.args),
                                              str(em.stdout.decode('utf-8'))))
            if em.returncode != 0:
                LOG.info('Failure while running extract-messages.sh on {}:\n'
                         '{}'.format(source_package.package_name,
                                     str(em.stdout)))
        except subprocess.SubprocessError as exc:
            LOG.info('Exception while running extract-messages.sh on {}:\n{}'.
                     format(source_package.package_name, str(exc)))

        try:
            Path(source_package.source_dir, 'messages.log').unlink()
        except IOError as exc:
            LOG.debug('Error while removing messages.log: {}'.format(str(exc)))

        enpo_dir = Path(templates_dir, 'en')
        enpo_dir.mkdir(exist_ok=True)
        all_pots = copy_track_files(Path(tmp_po_dir), Path(templates_dir),
                                    '*.pot')
        copy_track_files(Path(tmp_enpo_dir), Path(enpo_dir), '*.po')
        shutil.rmtree(tmp_po_dir, ignore_errors=True)
        shutil.rmtree(tmp_enpo_dir, ignore_errors=True)
        return all_pots
