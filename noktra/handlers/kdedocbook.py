# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>

import logging
import pathlib
import subprocess
from typing import Optional, List

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage

LOG = logging.getLogger(__name__)


def parse_legacy_doc_path(fp, module=None):
    """ Parse a file-like object which points to a documentation_paths file."""

    modules_paths = {}

    for line in fp:
        line = line.strip()

        # ignore empty lines and comments
        if (len(line) == 0) or line.startswith('#'):
            continue

        line_items = [el.strip() for el in line.split()]

        if line_items[0] != 'entry':
            continue
        if (len(line_items) < 3) or (len(line_items) > 4):
            continue

        entry_dir = line_items[1]
        module_name = line_items[2]
        if module and module_name != module:
            continue

        doc_path = line_items[3] if (len(line_items) > 3) else ''
        if module_name not in modules_paths.keys():
            modules_paths[module_name] = {}
        modules_paths[module_name][entry_dir] = doc_path

    return modules_paths


class KDEDocBookHandler(BaseHandler):

    _name = 'KDEDocBook'
    _configuration_key = 'kdedocbook'

    def default_included_re(self) -> List[str]:
        return [r'.*\.docbook$']

    def default_excluded_re(self) -> List[str]:
        return [r'.*_original.docbook$']

    def _run_docbookxml2pot(self, docbook, docbook_pot_path):
        try:
            x2d = subprocess.run(['xml2pot', str(docbook)],
                                 capture_output=True)
            LOG.debug('Executed {}'.format(str(x2d.args)))
            if x2d.returncode != 0:
                LOG.info('Failure while running xml2pot on {}:\n'
                         '{}'.format(str(docbook), str(x2d.stdout)))
            with open(docbook_pot_path, 'w') as dpotf:
                dpotf.write(x2d.stdout.decode('utf-8'))
        except subprocess.SubprocessError as exc:
            LOG.info('Exception while running xml2pot on {}:\n{}'.
                     format(str(docbook), str(exc)))

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:
        doc_templates = pathlib.Path(templates_dir, 'docs')
        doc_templates.mkdir(parents=True, exist_ok=True)
        modules_paths = {}

        # read the location of documentation from in-code configuration
        doc_paths = source_config.get('documentation_paths', {})
        if doc_paths:
            modules_paths[source_package.package_name] = doc_paths
        else:
            # fall-back to the legacy documentation configuration
            legacy_doc_path_cfg = self.config('legacy_documentation_paths',
                                              'documentation_paths')
            try:
                with open(legacy_doc_path_cfg, 'r') as fp:
                    modules_paths = parse_legacy_doc_path(
                        fp, source_package.package_name)
            except Exception:
                pass

        filtered_docbooks = self.filter_source_files(
            source_package.source_files)

        all_pots = []
        # find all relevant docbook files from sources
        for mname, mpath in modules_paths.get(source_package.package_name,
                                              {}).items():
            source_pattern = str(pathlib.Path(source_package.source_dir,
                                              mpath))
            # FIXME: the loops can be swapped (going through all source_files
            # once), and optimized, but the complexity would be the same
            for docbook in filtered_docbooks:
                if not str(docbook).startswith(source_pattern):
                    continue

                # build the name of the expected pot file
                docbook_rel = docbook.relative_to(source_package.source_dir)

                # TODO: use a regex instead of replace for the final
                # .docbook/index.docbook file
                docbook_pot = (str(pathlib.PurePath(
                    mname, docbook_rel.relative_to(mpath)))
                    .replace('/index.docbook', '')
                    .replace('.docbook', '')
                    .replace('/', '_')) + '.pot'

                # TODO: if check requested, and the file is not a valid
                # docbook, skip this file

                docbook_pot_path = pathlib.Path(doc_templates,
                                                docbook_pot)

                self._run_docbookxml2pot(docbook, docbook_pot_path)
                all_pots.append('docs/' + docbook_pot)
        return all_pots
