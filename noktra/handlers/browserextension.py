# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Luigi Toscano <luigi.toscano@tiscali.it>

import json
import logging
from pathlib import Path
from typing import Optional, List

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage
from noktra.lib.translationdata import TranslationTemplateData

LOG = logging.getLogger(__name__)


class BrowserExtensionHandler(BaseHandler):

    _name = 'BrowserExtension'
    _configuration_key = 'browserextension'

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        conf_messages_file = source_config.get('messages_file', None)

        if not conf_messages_file:
            return []

        messages_file = source_package.source_dir.joinpath(conf_messages_file)

        if catalog_name:
            pot_file_base = str(catalog_name)
        else:
            pot_file_base = source_package.package_name

        j_messages = {}
        try:
            with open(messages_file, 'r') as json_file:
                j_messages = json.load(json_file)
        except Exception as exc:
            LOG.debug('Error accessing {}, skipping: {}'.format(json_file,
                                                                str(exc)))
        # order the items based on the entry ID
        ordered_messages = dict(sorted(j_messages.items()))

        translation_items = TranslationTemplateData()
        for entry_id in ordered_messages:
            data = ordered_messages[entry_id]
            data_message = data.get('message', None)
            data_description = data.get('description', None)

            if not data_message:
                continue

            data_message = data_message.replace('\\n', '\n')
            translation_items.add_entry_data(
                data_description, data_message, entry_id, 1)

        out_translation_file = Path(
            templates_dir,
            pot_file_base + '._static_.pot')
        real_pot = translation_items.save_pot_file(
            out_translation_file,
            pot_file_base)
        if real_pot:
            return [real_pot]
        else:
            return []
