# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2022-2023 Luigi Toscano <luigi.toscano@tiscali.it>

import logging
import pathlib
from typing import Optional, List

import iniparse  # type: ignore

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage
from noktra.lib.translationdata import TranslationTemplateData

LOG = logging.getLogger(__name__)

TRANSLATED_KEYS = set([
    'Name',
    'Comment',
    'Language',
    'Keywords',
    'About',
    'Description',
    'GenericName',
    'Query',
    'ExtraNames',
    'Title',
    'X-KDE-Keywords',
    'X-KDE-Submenu',
])


class DesktopHandler(BaseHandler):
    """ This code saves and use also the key as context, which xgettext
    does not consider when dealing with .desktop files.
    """

    _name = 'Desktop'
    _configuration_key = 'desktop'

    _source_config_is_list = ['extra_desktop']

    def default_included_re(self) -> List[str]:
        return [r'.+\.desktop(\.cmake|\.in)?$',
                r'.+\.directory$',
                r'.+\.notifyrc$',
                r'.+\.knsrc$',
                ]

    def default_excluded_re(self) -> List[str]:
        return [r'.*\/tests?\/.*$',
                r'.*\/autotests?\/.*',
                r'.*APPNAMELC.*'
                ]

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        # additional patterns for desktop files
        extra_desktop_files = source_config.get('extra_desktop')

        filtered_desktop = self.filter_source_files(
            source_package.source_files,
            extra_included_re=extra_desktop_files)

        # TODO: allow for extracting into different pot files
        desktop_pot = source_package.package_name + '._desktop_.pot'

        all_translated_keys = TRANSLATED_KEYS.copy()
        # read the additional keys to be translated from the in-code
        # configuration
        extra_translated_keys = source_config.get('translated_keywords', [])
        # if not isinstance(extra_translated_keys, list):
        #     extra_translated_keys = [extra_translated_keys]
        all_translated_keys.update(extra_translated_keys)

        found_ctxtkeys = TranslationTemplateData()
        for desktop in filtered_desktop:
            parsed_desktop = None
            try:
                with open(desktop, 'r') as df:
                    # optionxformvalue=None preserves case when reading keys
                    parsed_desktop = iniparse.INIConfig(df,
                                                        optionxformvalue=None)
            except Exception:
                pass
            if not parsed_desktop:
                continue
            rel_desktop_path = desktop.relative_to(source_package.source_dir)

            # FIXME: fix iniparse to get the raw comments in a proper way
            current_context = ''
            for d_section in parsed_desktop:
                for d_line in (parsed_desktop[d_section]._lines)[0].contents:
                    if isinstance(d_line, iniparse.ini.CommentLine):
                        if d_line.comment.startswith(' ctxt:'):
                            current_context = d_line.comment[6:].strip()

                    if isinstance(d_line, iniparse.ini.LineContainer):
                        if d_line.name not in all_translated_keys:
                            continue

                        if current_context:
                            msg_ctxt = '{}|{}'.format(d_line.name,
                                                      current_context)
                        else:
                            msg_ctxt = d_line.name
                        # FIXME: right now the code retrieve the line number
                        # only if iniparse supports it, but as soon as a new
                        # version of iniparse with the appropriate patch is
                        # available, just depend on it and remove the check.
                        msg_line_number = getattr(d_line, 'line_number', None)
                        found_ctxtkeys.add_entry_data(
                            msg_ctxt, d_line.value, str(rel_desktop_path),
                            line_number=msg_line_number)

                        # found a real line, reset the context
                        current_context = ''

        out_desktop_file = pathlib.Path(templates_dir, desktop_pot)
        real_pot = found_ctxtkeys.save_pot_file(
            out_desktop_file,
            '{} desktop files'.format(source_package.package_name))
        if real_pot:
            return [real_pot]
        else:
            return []
