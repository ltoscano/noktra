# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>

from abc import ABC
from abc import abstractmethod
import logging
from pathlib import Path
import re
from typing import Iterable, List, Optional

from noktra.lib.sourcepackage import SourcePackage

LOG = logging.getLogger(__name__)


class BaseHandler(ABC):
    """ Base handler class."""

    _name = ''
    _configuration_key = ''
    _support_multiple_calls = False

    # source configuration variables which must be a list
    _source_config_is_list: List[str] = []

    def __init__(self, configuration=None):
        self._configuration = configuration
        pass

    def __str__(self):
        return self._name

    def config(self, key, fallback=None):
        if self._configuration_key not in self._configuration:
            self._configuration_key = 'DEFAULT'
        value = self._configuration[self._configuration_key].get(key, fallback)
        return value

    @property
    def support_multiple_calls(self):
        return self._support_multiple_calls

    def sanitize_source_config(self, handler_source_config: dict):
        """ Check and sanitize the source configuration.
        Try to fix and cleanup as much as possible,
        the extraction should continue as much as possible.
        """
        # TODO: implement the definition of the configuration keys
        # in a more general way, maybe like oslo.config.
        for expected_list in self._source_config_is_list:
            if not handler_source_config.get(expected_list):
                continue
            candidate_list = handler_source_config.get(expected_list)
            if not isinstance(candidate_list, list):
                handler_source_config[expected_list] = [candidate_list]
                LOG.info('Sanitized source configuration: {} is a list'.
                         format(expected_list))

    def default_included_re(self) -> List[str]:
        return []

    def default_excluded_re(self) -> List[str]:
        return []

    def filter_source_files(self, source_files: List[Path],
                            extra_included_re: Optional[Iterable[str]] = None,
                            extra_excluded_re: Optional[Iterable[str]] = None,
                            specific_files: Optional[Iterable[str]] = None
                            ) -> List[Path]:
        """ Filter the list of files which wll be parsed """
        # TODO: read from the configuration and get the list of patterns
        # which should be added or removed.
        all_included_re = self.default_included_re().copy()
        if extra_included_re:
            all_included_re.extend(extra_included_re)
        all_excluded_re = self.default_excluded_re().copy()
        if extra_excluded_re:
            all_excluded_re.extend(extra_excluded_re)

        included_re = [re.compile(r) for r in all_included_re]
        excluded_re = [re.compile(r) for r in all_excluded_re]

        filtered_files = []

        for candidate_file in source_files:
            explicitely_excluded = False
            explicitely_included = False
            for exre in excluded_re:
                if exre.match(str(candidate_file)):
                    explicitely_excluded = True
                    break
            # not good, it hit an exclusion regex
            if explicitely_excluded:
                continue

            for inre in included_re:
                if inre.match(str(candidate_file)):
                    explicitely_included = True
                    break
            # return the file, it is part of an inclusion regex
            # and it isn't part of any exclusion regex at this point;
            # but if specific files are specified,
            # check also if it's part of that set
            if explicitely_included:
                if (not specific_files) or \
                        (candidate_file.name in specific_files):
                    filtered_files.append(candidate_file)
        LOG.debug('Filtered source files (unsorted): {}'.format(
            [str(f) for f in filtered_files]))
        return sorted(filtered_files)

    @abstractmethod
    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:
        # TODO: return a List[str] (list of extracted templates)
        pass


class BaseInjectorHandler(BaseHandler):
    """ Base class for handlers which can also inject translations back."""

    @abstractmethod
    def inject_translations(self, source_package: SourcePackage,
                            translations_dir: str, source_config: dict,
                            catalog_name: Optional[str] = None) -> List[str]:
        pass
