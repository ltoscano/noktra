# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Luigi Toscano <luigi.toscano@tiscali.it>

import logging
from os import environ
import subprocess
from typing import Optional, List

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage

LOG = logging.getLogger(__name__)


class AndroidHandler(BaseHandler):

    _name = 'Android'
    _configuration_key = 'android'

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        if catalog_name:
            local_catalog_name = str(catalog_name)
        else:
            local_catalog_name = str(source_package.package_name)
        full_catalog_name = '{}._static_'.format(local_catalog_name)

        resource_dir = source_config.get('resources', None)
        # no resource directory specified, do not extract anything
        if not resource_dir:
            return []

        a2po_env = environ.copy()
        a2po_env['ANSI_COLORS_DISABLED'] = '1'

        all_pots = []
        try:
            a2pocmd = subprocess.run(['a2po', 'export', '-q',
                                      '--android', resource_dir,
                                      '--gettext', templates_dir,
                                      '--domain', full_catalog_name],
                                     capture_output=True,
                                     env=a2po_env,
                                     cwd=source_package.source_dir)
            LOG.debug('Executed {}'.format(str(a2pocmd.args)))
            if a2pocmd.returncode != 0:
                LOG.info('Failure while running a2po extract:\n'
                         '{}'.format(str(a2pocmd.stderr)))
            else:
                all_pots.append('{}.pot'.format(full_catalog_name))
            LOG.debug('{}'.format(str(a2pocmd.stdout)))
        except subprocess.SubprocessError as exc:
            LOG.info('Exception while running a2po extract:\n{}'.
                     format(str(exc)))

        return all_pots
