# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021-2023 Luigi Toscano <luigi.toscano@tiscali.it>

from typing import Optional, List, Tuple

from noktra.handlers.base_xml import BaseXMLHandler


class MetainfoHandler(BaseXMLHandler):

    _name = 'Metainfo'
    _configuration_key = 'metainfo'
    _xml_namespace = ''

    def default_included_re(self) -> List[str]:
        return [r'.+\.(metainfo|appdata)\.xml$']

    def translatable_patterns(
            self, config_tags: List[str]
            ) -> List[Tuple[str, Optional[str]]]:
        patterns = ['/component/name',
                    '/component/summary',
                    '/component/description',
                    '/component/screenshots/screenshot/caption',
                    '/component/developer_name']
        if config_tags and isinstance(config_tags, list):
            patterns.extend(config_tags)
        return [(pattern, None) for pattern in patterns]

    def default_source_file(self, package_name) -> Optional[str]:
        # No default file name: rely on each metainfo file found
        return None
