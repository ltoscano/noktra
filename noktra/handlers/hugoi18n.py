# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Luigi Toscano <luigi.toscano@tiscali.it>

import logging
from pathlib import Path
from os import environ
from shutil import rmtree
import subprocess
import tempfile
from typing import Optional, List

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage
from noktra.lib.fileutils import copy_track_files

LOG = logging.getLogger(__name__)


class HugoI18nHandler(BaseHandler):

    _name = 'HugoI18n'
    _configuration_key = 'hugoi18n'

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        tmp_po_dir = Path(tempfile.mkdtemp(prefix='noktra.hi18n.po'))

        hugoi18n_env = environ.copy()
        hugoi18n_env['PACKAGE'] = source_package.package_name
        LOG.info('PACKAGE: {}'.format(hugoi18n_env['PACKAGE']))

        try:
            hugoi18ncmd = subprocess.run(['hugoi18n', 'extract', tmp_po_dir],
                                         capture_output=True,
                                         env=hugoi18n_env,
                                         cwd=source_package.source_dir)
            LOG.debug('Executed {}'.format(str(hugoi18ncmd.args)))
            if hugoi18ncmd.returncode != 0:
                LOG.info('Failure while running hugoi18n extract:\n'
                         '{}'.format(str(hugoi18ncmd.stderr)))
            LOG.debug('{}'.format(str(hugoi18ncmd.stdout)))
        except subprocess.SubprocessError as exc:
            LOG.info('Exception while running hugoi18n extract:\n{}'.
                     format(str(exc)))

        all_pots = copy_track_files(tmp_po_dir, Path(templates_dir), '*.pot')

        rmtree(tmp_po_dir, ignore_errors=True)

        return all_pots
