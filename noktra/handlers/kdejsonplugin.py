# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2022 Luigi Toscano <luigi.toscano@tiscali.it>

import json
import logging
import pathlib
from typing import Optional, List

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage
from noktra.lib.translationdata import TranslationTemplateData

LOG = logging.getLogger(__name__)

KPLUGIN_MAIN_TRANSLATED_KEYS = [
    'Name',
    'Description',
    'Copyright',
    'ExtraInformation',
]


KPLUGIN_AUTHORSHIP_KEYS = [
    'Authors',
    'Translators',
    'OtherContributors',
]


# Translatable fields as defined in KAboutPerson::fromJSON()
KPLUGIN_AUTHORSHIP_FIELDS = [
    'Name',
    'Task',
]


class KDEJSONPluginHandler(BaseHandler):

    _name = 'KDEJSONPlugin'
    _configuration_key = 'kdejsonplugin'

    _source_config_is_list = ['extra_keywords']

    def default_included_re(self) -> List[str]:
        return [r'.+\.json(\.cmake|\.in)?$']

    def default_excluded_re(self) -> List[str]:
        return [r'.*\/tests?\/.*$',
                r'.*\/autotests?\/.*$']

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        filtered_jsons = self.filter_source_files(source_package.source_files)

        kplugin_all_translated_keys = KPLUGIN_MAIN_TRANSLATED_KEYS.copy()
        # read the additional keys to be translated from the in-code
        # configuration
        extra_translated_keys = source_config.get('extra_keywords', [])
        kplugin_all_translated_keys.extend(extra_translated_keys)

        kplugin_all_authorship_keys = KPLUGIN_AUTHORSHIP_KEYS.copy()

        translation_items = TranslationTemplateData()

        for json_file in filtered_jsons:
            rel_json_location = str(json_file.relative_to(
                source_package.source_dir))
            json_content = {}
            try:
                with open(json_file, 'r') as jfp:
                    json_content = json.load(jfp, strict=False)
                    if ('KPlugin' not in json_content) and \
                            ('KDE-KIO-Protocols' not in json_content):
                        # not a recognized translatable json file
                        json_content = {}
                        continue
            except Exception as exc:
                LOG.debug('Error accessing {}, skipping: {}'.format(json_file,
                                                                    str(exc)))

            if 'KDE-KIO-Protocols' in json_content:
                kde_kio_protocols_content = json_content['KDE-KIO-Protocols']
                for key in kde_kio_protocols_content.keys():
                    extra_names = kde_kio_protocols_content[key].get(
                        'ExtraNames')
                    if not extra_names:
                        continue
                    # FIXME: maybe escape? Or should polib take care of it?
                    translation_items.add_entry_data(
                        key + ' ExtraNames', ';'.join(extra_names),
                        rel_json_location)

            if 'KPlugin' in json_content:
                kplugin_content = json_content['KPlugin']
                for key in kplugin_all_translated_keys:
                    if key in kplugin_content:
                        translation_items.add_entry_data(
                            key, kplugin_content[key], rel_json_location)
                        continue
                    # check for keywords also outside KPlugin
                    if key in json_content:
                        translation_items.add_entry_data(
                            key, json_content[key], rel_json_location)
                        continue
                for authorship_key in kplugin_all_authorship_keys:
                    if authorship_key not in kplugin_content:
                        continue
                    # an "authorship" field contains a list of author data
                    # including the translatable KPLUGIN_AUTHORSHIP_FIELDS
                    for author in kplugin_content[authorship_key]:
                        for author_field in KPLUGIN_AUTHORSHIP_FIELDS:
                            if author_field not in author:
                                continue
                            translation_items.add_entry_data(
                                '{} {}'.format(authorship_key, author_field),
                                author[author_field],
                                rel_json_location)

        out_json_file = pathlib.Path(
            templates_dir,
            source_package.package_name + '._json_.pot')
        real_pot = translation_items.save_pot_file(
            out_json_file,
            '{} json files'.format(source_package.package_name))
        if real_pot:
            return [real_pot]
        else:
            return []
