# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Luigi Toscano <luigi.toscano@tiscali.it>

from concurrent.futures import ProcessPoolExecutor
import logging
from pathlib import Path
from shutil import copyfile
import subprocess
from typing import Optional, List

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage

LOG = logging.getLogger(__name__)


class SphinxHandler(BaseHandler):

    _name = 'Sphinx'
    _configuration_key = 'sphinx'

    _source_config_is_list = ['excluded_pages']

    def default_included_re(self) -> List[str]:
        return [r'.*\.rst$']

    def _run_sphinx(self, source_dir):
        try:
            mg = subprocess.run(['make', 'gettext'],
                                capture_output=True,
                                cwd=source_dir)
            LOG.debug('Executed {}'.format(str(mg.args)))
            if mg.returncode != 0:
                LOG.info('Failure while running make gettext:\n'
                         '{}'.format(str(mg.stderr)))
            LOG.debug('{}'.format(str(mg.stdout)))
        except subprocess.SubprocessError as exc:
            LOG.info('Exception while running make gettext:\n{}'.
                     format(str(exc)))

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:

        excluded_pages = set(source_config.get('excluded_pages', []))
        default_pot_prefix = source_package.package_name.replace('-', '_')
        pot_prefix = source_config.get('pot_prefix', default_pot_prefix)
        dir_separator = source_config.get('dir_separator', '___')
        build_dir = source_config.get('build_dir', 'build/gettext')

        self._run_sphinx(source_package.source_dir)

        generated_pot_dir = Path(source_package.source_dir).joinpath(
            build_dir)

        all_pots = []
        src_pots = []
        dst_pots = []
        # find the pot files, excluding the excluded ones,
        # and compute the final pot name
        for pot in Path(generated_pot_dir).glob('**/*.pot'):
            pot_relative = pot.relative_to(generated_pot_dir)

            pot_parents = set(str(pot_relative.parent).split('/'))
            if (pot_parents.intersection(excluded_pages)) or \
                (pot_relative.name in excluded_pages):
                LOG.info('Excluded item found in {}, skipping...'.format(
                    pot_relative))
                continue

            new_pot = '{}_{}'.format(
                pot_prefix,
                str(pot_relative).replace('/', dir_separator))
            new_pot_path = Path(templates_dir).joinpath(Path(new_pot))
            src_pots.append(pot)
            dst_pots.append(new_pot_path)
            all_pots.append(new_pot)

        # copy all the pot files to the destination directory with
        # the correct name
        with ProcessPoolExecutor() as executor:
            executor.map(copyfile, src_pots, dst_pots)
        return all_pots
