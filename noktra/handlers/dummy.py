# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>

from noktra.handlers.base import BaseHandler
from noktra.lib.sourcepackage import SourcePackage
from typing import List, Optional


class DummyHandler(BaseHandler):

    _name = 'Dummy'
    _configuration_key = 'dummy'

    def extract_templates(self, source_package: SourcePackage,
                          templates_dir: str, source_config: dict,
                          catalog_name: Optional[str] = None) -> List[str]:
        return []
