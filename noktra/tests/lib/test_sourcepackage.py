# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2022 Luigi Toscano <luigi.toscano@tiscali.it>

from pathlib import Path

from noktra.lib.sourcepackage import SourceConfiguration
from noktra.lib.sourcepackage import SourceConfigurationReader
from noktra.lib.sourcepackage import SOURCE_CONFIG_FILE_NAME


class TestSourceConfiguration():

    def test_sourceconfigurationreader_base(self, mocker):
        sp = mocker.patch('noktra.lib.sourcepackage.SourcePackage')
        sp.source_dir = '/tmp/sources'
        sp.source_files = [Path('/tmp/sources/foo.c'),
                           Path('/tmp/sources/', SOURCE_CONFIG_FILE_NAME)]
        scr = SourceConfigurationReader(sp)
        mocker.patch('builtins.open', mocker.mock_open(
            read_data=b"""
            [handler1]
            k1 = 'v1'

            [handler2]
            k1 = 'v1b'
            k2 = 'v2'

            [handler2-foo]
            k1 = 'v1c'
            """))
        ci = scr.config_items
        assert ci == \
            [SourceConfiguration('handler1', None, config={'k1': 'v1'}),
             SourceConfiguration('handler2', None, config={'k1': 'v1b',
                                                           'k2': 'v2'}),
             SourceConfiguration('handler2', 'foo', config={'k1': 'v1c'})]
