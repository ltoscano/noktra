# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2022 Luigi Toscano <luigi.toscano@tiscali.it>

from noktra.lib.translationdata import TranslationTemplateData


def test_translationdata_add_noctxt():
    ttd = TranslationTemplateData()

    ttd.add_entry_data(None, 'Something', 'foo.cpp')

    assert list(ttd.get_keys()) == [(None, 'Something')]
    assert ttd.get_location(None, 'Something') == [('foo.cpp', '')]
    assert ttd.get_comment(None, 'Something') == ''


def test_translationdata_get_non_existing():
    ttd = TranslationTemplateData()

    assert ttd.get_location(None, 'Non existing') == []
    assert ttd.get_comment(None, 'Non existing') == ''


def test_translationdata_add_multiple():
    ttd = TranslationTemplateData()

    ttd.add_entry_data('Name', 'A program', 'foo.cpp', 3)
    ttd.add_entry_data('Comment', 'It does something', 'foo.cpp', 4, 'sooo')
    ttd.add_entry_data('Name', 'SuperDuper', 'bar.cpp', 10)
    ttd.add_entry_data('Name', 'A program', 'bar.cpp', 15)
    ttd.add_entry_data(None, 'And this is a msg', 'baz.cpp')

    expected_keys = [
        ('Name', 'A program'),
        ('Comment', 'It does something'),
        ('Name', 'SuperDuper'),
        (None, 'And this is a msg')
    ]
    assert set(ttd.get_keys()) == set(expected_keys)
    assert len(ttd.get_keys()) == len(expected_keys)

    assert ttd.get_location('Name', 'A program') == [
        ('bar.cpp', 15), ('foo.cpp', 3)]
    assert ttd.get_comment('Name', 'A program') == ''
    assert ttd.get_location('Comment', 'It does something') == [
        ('foo.cpp', 4)]
    assert ttd.get_comment('Comment', 'It does something') == 'sooo'
    assert ttd.get_location('Name', 'SuperDuper') == [('bar.cpp', 10)]
    assert ttd.get_comment('Name', 'SuperDuper') == ''
    assert ttd.get_location(None, 'And this is a msg') == [('baz.cpp', '')]
    assert ttd.get_comment(None, 'And this is a msg') == ''
