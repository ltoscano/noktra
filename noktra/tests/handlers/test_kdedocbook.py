# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Luigi Toscano <luigi.toscano@tiscali.it>

import io

from noktra.handlers.kdedocbook import parse_legacy_doc_path


DATA_DOCUMENTATION_PATHS_EMPTY = """

"""

DATA_DOCUMENTATION_PATHS_COMPLEX = """

entry mod1 p2 p3 p4 p5 p5

# ignore me

emp mod1

entry outmod2 mod2

entry outmod2

entry outmod3 mod3 doc/dir3
entry outmod3
  # ignore me too

entry outmod4 mod4

entry outmod2bis mod2 doc/dir2bis
"""

RESULT_DOCUMENTATION_PATHS_COMPLEX = {
    'mod2': {
        'outmod2': '',
        'outmod2bis': 'doc/dir2bis'
    },
    'mod3': {
        'outmod3': 'doc/dir3'
    },
    'mod4': {
        'outmod4': ''
    },
}

RESULT_DOCUMENTATION_PATHS_COMPLEX_MOD2 = {
    'mod2': {
        'outmod2': '',
        'outmod2bis': 'doc/dir2bis'
    },
}


def test_parse_legacy_documentation_paths_empty():
    fp = io.StringIO(DATA_DOCUMENTATION_PATHS_EMPTY)
    assert parse_legacy_doc_path(fp) == {}


def test_parse_legacy_documentation_paths_complex():
    fp = io.StringIO(DATA_DOCUMENTATION_PATHS_COMPLEX)
    assert parse_legacy_doc_path(fp) == RESULT_DOCUMENTATION_PATHS_COMPLEX


def test_parse_legacy_documentation_paths_complex_singlemod():
    fp = io.StringIO(DATA_DOCUMENTATION_PATHS_COMPLEX)
    mod2_paths = parse_legacy_doc_path(fp, 'mod2')
    assert mod2_paths == RESULT_DOCUMENTATION_PATHS_COMPLEX_MOD2
